﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using DG.Tweening;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    public int playerCount = 0;


    [SerializeField]
    Color[] teamColors = new Color[4];

    [SerializeField]
    GameObject playerPrefab;
    public PlayerController[] players;

    [SerializeField]
    ScoreManager score;
    [SerializeField]
    SceneManagement sceneManager;
    [SerializeField]
    SpawnManager spawner;

    [SerializeField]
    Transform[] initialSpawnPoints;


    [SerializeField]
    float matchLength;
    [SerializeField]
    float currentGameTime;

    [SerializeField]
    bool gameStarted;


    [SerializeField]
    bool matchStart;

    public LookScript cameraLook;
    public bool focusOnWinner;

    #region Setup

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this);
        }

        DontDestroyOnLoad(this);

        players = new PlayerController[4];

    }
    private void Start()
    {
        cameraLook = Camera.main.GetComponent<LookScript>();
        GameSetup(false);
    }

    public void GameSetup(bool resetCamera)
    {
        playerCount = 0;
        currentGameTime = 0;
        focusOnWinner = false;

        if (resetCamera)
        {
            cameraLook.ResetPosition();
        }

        if (score != null)
        {
            score.Setup(4);
        }

    }

    public void StartGame()
    {
        gameStarted = true;
    }

    public void StartMatch()
    {
        matchStart = true;
    }

    public void SpawnPlayer(int playerIndex, XboxController controller)
    {
        if (playerCount < 4 && players[playerIndex - 1] == null)
        {
            GameObject player = Instantiate(playerPrefab, initialSpawnPoints[playerIndex - 1].transform.position, Quaternion.identity);
            player.transform.parent = null;
            player.GetComponent<PlayerController>().controller = controller;
            player.GetComponent<PlayerController>().playerNumber = playerIndex - 1;
            players[playerIndex - 1] = player.GetComponent<PlayerController>();

            players[playerIndex - 1].SetColor(teamColors[playerCount]);

            SoundManager.instance.PlayOneShot(AudioDirectory.Scream);

            playerCount++;

        }
    }

    public void RetractSpawnPlatforms()
    {
        SoundManager.instance.PlayOneShot(AudioDirectory.RockSlide);

        foreach (Transform spawn in initialSpawnPoints)
        {
            float height = spawn.parent.localScale.y / 2;
            float yPos = spawn.parent.position.y;
            spawn.parent.DOScaleY(0.1f, 4);
            spawn.parent.DOMoveY(yPos - height + 0.1f, 4).OnComplete(disablePlatforms);
        }
    }
    public void disablePlatforms()
    {
        SoundManager.instance.PlayOneShot(AudioDirectory.HawkScreech);

        foreach (Transform spawn in initialSpawnPoints)
        {
            //disable siblings
            foreach (Transform sibling in spawn.parent.GetComponentsInChildren<Transform>())
            {
                if (!sibling.CompareTag("boulder"))
                {
                    if (sibling != spawn && sibling != spawn.parent)
                    {
                        sibling.gameObject.SetActive(false);
                    }
                }
            }
            //disable collider
            spawn.parent.GetComponent<Collider>().enabled = false;
        }

    }

    #endregion

    #region GameState

    public void EndGame()
    {
        cameraLook.target = score.GetHighestScorer().transform;
        cameraLook.enabled = true;

        gameStarted = false;
        matchStart = false;
        focusOnWinner = true;
    }

    void Update()
    {
        CompletionCheck();
    }

    void CompletionCheck()
    {
        if (matchStart)
        {
            if (currentGameTime >= matchLength)
            {
                EndGame();
            }
            else
            {
                currentGameTime += Time.deltaTime;
            }

        }
    }

    #endregion

    public void Showdown(PlayerController attacker, PlayerController defender)
    {

        PlayerController[] winnerLoser = ShowdownOutcome(attacker, defender);

        if (winnerLoser == null)
        {
            attacker.BounceAway(defender.transform.position);
            defender.BounceAway(attacker.transform.position);
            return;
        }
        else
        {

            PlayerController winner = winnerLoser[0];
            PlayerController loser = winnerLoser[1];

            spawner.KillPlayer(loser);

            winner.SetAnimationTrigger(winner.GetAnimator(winner.transState.currentState), "Attack");
            winner.moveMotor.QuickLook(loser.transform.position - winner.transform.position);

            switch (winner.transState.currentState)
            {
                case TransformState.Rock:
                    SoundManager.instance.PlayOneShot(AudioDirectory.RockKill);
                    break;
                case TransformState.Paper:
                    SoundManager.instance.PlayOneShot(AudioDirectory.PaperKill);
                    break;
                case TransformState.Scissors:
                    SoundManager.instance.PlayOneShot(AudioDirectory.ScissorKill);
                    break;
            }


            if (matchStart)
            {
                score.AddScore(winner.playerNumber, 1);
            }
        }
    }



    PlayerController[] ShowdownOutcome(PlayerController attacker, PlayerController defender)
    {

        TransformState defenderState = defender.transState.currentState;

        switch (attacker.transState.currentState)
        {
            case (TransformState.Rock):
                if (defenderState == TransformState.Paper)
                {
                    return new PlayerController[2] { defender, attacker };
                }
                if (defenderState == TransformState.Scissors)
                {
                    return new PlayerController[2] { attacker, defender };
                }
                break;
            case (TransformState.Paper):
                if (defenderState == TransformState.Rock)
                {
                    return new PlayerController[2] { attacker, defender };
                }
                if (defenderState == TransformState.Scissors)
                {
                    return new PlayerController[2] { defender, attacker };
                }
                break;
            case (TransformState.Scissors):
                if (defenderState == TransformState.Rock)
                {
                    return new PlayerController[2] { defender, attacker };
                }
                if (defenderState == TransformState.Paper)
                {
                    return new PlayerController[2] { attacker, defender };
                }
                break;
        }

        return null;
    }

}
