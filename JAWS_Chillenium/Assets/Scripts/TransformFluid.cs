﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public enum TransformState { Rock, Paper, Scissors, None }

public class TransformFluid : MonoBehaviour
{
    public TransformState currentState;

    [SerializeField]
    UnityEvent rock;
    [SerializeField]
    UnityEvent paper;
    [SerializeField]
    UnityEvent scissors;

    [SerializeField]
    float cooldownTime;



    float timer;

    private void Update()
    {
        timer += Time.deltaTime;
    }

    public void ChangeState(TransformState newState)
    {
        if (CanChange())
        {
            if (newState == currentState)
                return;

            switch (newState)
            {
                case TransformState.Rock:
                    rock.Invoke();
                    break;
                case TransformState.Paper:
                    paper.Invoke();
                    break;
                case TransformState.Scissors:
                    scissors.Invoke();
                    break;
            }

            currentState = newState;
            timer = 0;
        }
    }

    bool CanChange()
    {
        return (timer > cooldownTime);
    }

}
