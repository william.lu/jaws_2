﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XboxCtrlrInput;
using UnityEngine.UI;

public class InputReceiver : MonoBehaviour
{

    public TransformFluid[] playerStates;

    [SerializeField]
    bool[] pressed;
    [SerializeField]
    Image barImage;

    public bool tutorialBegin;

    public bool canPressStart;

    bool slideComplete;

    public enum Phase
    {
        waitForAPress,
        Rock,
        Paper,
        Scissors
    }

    public Phase currentPhase = Phase.waitForAPress;

    float skipBarFillAmount = 0;

    private void Awake()
    {


    }

    // Use this for initialization
    void Start()
    {
        skipBarFillAmount = 0;
        barImage.fillAmount = skipBarFillAmount;
    }

    private void ResetValues()
    {

    }

    public void InitializeTutorial()
    {
        playerStates = new TransformFluid[4];

        for (int i = 0; i < playerStates.Length; i++)
        {
            if (GameManager.instance.players[i] != null)
                playerStates[i] = GameManager.instance.players[i].GetComponentInChildren<TransformFluid>();
        }


        pressed = new bool[GameManager.instance.playerCount];

        tutorialBegin = true;
    }


    public void TutorialPromptCheck()
    {
        //turned on by UIManager
        if (currentPhase == Phase.waitForAPress)
        {
            WaitForAPress();
        }
        else if (tutorialBegin)
        {
            switch (currentPhase)
            {

                case Phase.Rock:
                    CycleState(TransformState.Rock);
                    break;
                case Phase.Paper:
                    CycleState(TransformState.Paper);
                    break;
                case Phase.Scissors:
                    CycleState(TransformState.Scissors);
                    break;


            }
        }

    }




    void CycleState(TransformState state)
    {
        if (currentPhase.ToString() == state.ToString())
        {
            for (int i = 0; i < playerStates.Length; i++)
            {
                if (playerStates[i] != null)
                {
                    if (playerStates[i].currentState != state)
                    {
                        return;
                    }
                }


            }
            //cycle if everyone is a rock, and the current phase is rock
            currentPhase += 1;
        }
    }


    void WaitForAPress()
    {
        for (int i = 1; i < 5; i++)
        {
            //+1 because 0 is all i think
            if (XCI.GetButtonDown(XboxButton.A, (XboxController)i))
            {
                Debug.Log(i);
                //spawn the player
                GameManager.instance.SpawnPlayer(i, (XboxController)i);
                GameManager.instance.cameraLook.slideIn();

                if (slideComplete == false)
                {
                    SoundManager.instance.PlayOneShot(AudioDirectory.TumbleWeed);
                    slideComplete = true;
                }

                if (playerCount() >= 2)
                {
                    canPressStart = true;
                }
            }
        }

        /* checks if all players are ready
        foreach (bool playerPressed in pressed)
        {
            if (playerPressed == false)
            {
                return;
            }
        }
        */
        for (int i = 1; i < 5; i++)
        {
            if (GameManager.instance.players[i - 1] != null)
            {
                if (XCI.GetButtonDown(XboxButton.Start, (XboxController)i))
                {
                    if (canPressStart)
                    {
                        currentPhase += 1;
                        GameManager.instance.StartGame();
                    }
                }
            }
        }
    }



    public void HoldAToSkip()
    {
        if (tutorialBegin)
        {
            barImage.gameObject.SetActive(true);
            for (int i = 1; i < 5; i++)
            {
                if (XCI.GetButton(XboxButton.A, (XboxController)i))
                {
                    if (skipBarFillAmount >= 1)
                    {
                        //COUPLING CODE rip me
                        tutorialBegin = false;
                        StartCoroutine(GetComponent<UIManager>().skipTutorial());
                    }

                    //amount of time it takes to skip
                    skipBarFillAmount += Time.deltaTime / 2f;
                    barImage.fillAmount = skipBarFillAmount;
                }

                if (XCI.GetButtonUp(XboxButton.A, (XboxController)i))
                {
                    skipBarFillAmount = 0;
                    barImage.fillAmount = skipBarFillAmount;
                }

                /*
                if (XCI.GetButton(XboxButton.A, XboxController.Any))
                {
                    if (skipBarFillAmount >= 1)
                    {
                        //COUPLING CODE rip me
                        tutorialBegin = false;
                        StartCoroutine(GetComponent<UIManager>().skipTutorial());
                    }

                    //amount of time it takes to skip
                    skipBarFillAmount += Time.deltaTime / 1.5f;
                    barImage.fillAmount = skipBarFillAmount;
                }

                if (XCI.GetButtonUp(XboxButton.A, XboxController.Any))
                {
                    skipBarFillAmount = 0;
                    barImage.fillAmount = skipBarFillAmount;
                }
                */
            }

        }
    }

    public int playerCount()
    {
        int counter = 0;
        foreach (PlayerController player in GameManager.instance.players)
        {
            if (player != null)
            {
                counter++;
            }
        }

        return counter;
    }
}
