﻿using System.Collections;
using System.Collections.Generic;
using XboxCtrlrInput;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public XboxController controller;

    public StateAttack attackComponent;
    public TransformFluid transState;

    public MotorMove moveMotor;
    Rigidbody rbComponent;

    Vector3 direction;

    [SerializeField]
    float bumpForce = 25;

    [SerializeField]
    MeshRenderer rockColor;
    [SerializeField]
    MeshRenderer paperColor;
    [SerializeField]
    MeshRenderer scissorColor;

    [SerializeField]
    Animator rockAnimator;
    [SerializeField]
    Animator paperAnimator;
    [SerializeField]
    Animator scissorAnimator;

    public ParticleSystem spawnParticle;

    public ParticleSystem deathParticle;

    public GameObject spawnShield;

    public int playerNumber;

    public bool dead;

    private void Awake()
    {
        attackComponent = GetComponent<StateAttack>();
        transState = GetComponent<TransformFluid>();
        moveMotor = GetComponent<MotorMove>();
        rbComponent = GetComponent<Rigidbody>();
    }


    void Update()
    {
        InputUpdate();
        AnimationUpdate();
    }

    private void FixedUpdate()
    {
        moveMotor.MoveDirection(direction, rbComponent);
        moveMotor.LookDirection(direction, rbComponent);
    }

    public void SetAnimationTrigger(Animator anim, string trigger)
    {
        anim.SetTrigger(trigger);
    }

    void AnimationUpdate()
    {
        GetAnimator(transState.currentState).SetFloat("Movement", rbComponent.velocity.magnitude);
    }

    public Animator GetAnimator(TransformState state)
    {
        switch (state)
        {
            case (TransformState.Rock):
                return rockAnimator;
            case (TransformState.Paper):
                return paperAnimator;
            case (TransformState.Scissors):
                return scissorAnimator;
            default:
                return null;
        }
    }

    public void SetColor(Color teamColor)
    {
        rockColor.materials[1].color = teamColor;
        paperColor.materials[0].color = teamColor;
        scissorColor.materials[0].color = teamColor;
    }

    #region INPUT

    void InputUpdate()
    {
        UpdateDirection();
        UpdateTransformation();
    }


    public void BounceAway(Vector3 awayFromObj)
    {
        moveMotor.MoveDirection((transform.position - awayFromObj).normalized * bumpForce, rbComponent);
        moveMotor.QuickLook(transform.position - awayFromObj);
    }

    void UpdateTransformation()
    {
        if (XCI.GetButtonDown(XboxButton.B, controller))
        {
            transState.ChangeState(TransformState.Rock);
        }
        if (XCI.GetButtonDown(XboxButton.Y, controller))
        {
            transState.ChangeState(TransformState.Paper);
        }
        if (XCI.GetButtonDown(XboxButton.X, controller))
        {
            transState.ChangeState(TransformState.Scissors);
        }
    }

    void UpdateDirection()
    {
        Vector2 input = new Vector2(XCI.GetAxis(XboxAxis.LeftStickX, controller), XCI.GetAxis(XboxAxis.LeftStickY, controller));
        direction = Camera.main.transform.parent.TransformDirection(new Vector3(input.x, 0, input.y));
        direction.y = 0;
        direction = direction.normalized;
    }

    #endregion

    public void Attacked(PlayerController attackType)
    {
        GameManager.instance.Showdown(attackType, this);
    }
}
