﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LookScript : MonoBehaviour {

    public Transform target;
    public float rotSpeed = 2;

    public Vector3 initialPosition;
    public Vector3 initialRotation;

    Transform startPositionCamera;

    bool inPosition;

    private void Awake()
    {
        initialPosition = transform.position;
        initialRotation = transform.localEulerAngles;
        startPositionCamera = transform.parent.GetChild(1);
    }

    void LateUpdate() { 
        if (target != null)
        {
            
           transform.forward = Vector3.Lerp(transform.forward, Vector3.RotateTowards(transform.forward, target.position - transform.position, rotSpeed, 0.0f), 0.1f);
        }


	}
    private void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            slideIn();
        }
    }


    public void ResetPosition()
    {
        target = null;
        transform.DORotate(initialRotation, 3).SetEase(Ease.OutBack);
        transform.DOMove(initialPosition, 3).SetEase(Ease.OutBack);
    }

    public void Start()
    {
        transform.position = startPositionCamera.position;
        transform.rotation = startPositionCamera.rotation;
    }

    public void slideIn()
    {
        if (!inPosition)
        {
            inPosition = true;
            transform.DORotate(initialRotation, 6).SetEase(Ease.InOutCubic);
            transform.DOMove(initialPosition, 6).SetEase(Ease.InOutCubic);
        }
    }
}
