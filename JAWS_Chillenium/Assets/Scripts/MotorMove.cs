﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotorMove : MonoBehaviour
{
    [SerializeField]
    float speed;

    [SerializeField]
    float maxSpeed;

    [SerializeField]
    float rotationSpeed;

    public void MoveDirection(Vector3 direction, Rigidbody rbComponent)
    {
        if (rbComponent.velocity.magnitude > maxSpeed || direction == Vector3.zero)
        {
            rbComponent.velocity /= 1.2f;
        }
        else
        {
            rbComponent.AddForce(direction * (speed * Time.deltaTime), ForceMode.Impulse);
        }
    }

    public void LookDirection(Vector3 direction, Rigidbody rbComponent)
    {
        Vector3 newDir = Vector3.RotateTowards(transform.forward, direction, (rotationSpeed * Time.deltaTime), 0.0f);
        rbComponent.MoveRotation(Quaternion.LookRotation(newDir));
    }

    public void QuickLook(Vector3 direction)
    {
        transform.forward = direction;
    }

}
