﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{

    [SerializeField]
    int[] elimCount;
    [SerializeField]
    int scoreToWin = 5;

    public void Setup(int players)
    {
        elimCount = new int[players];
    }

    public void AddScore(int playerID, int scoreAmount)
    {
        elimCount[playerID] += scoreAmount;
        CheckForEndGame(playerID);
    }

    private void CheckForEndGame(int playerID)
    {
        if (elimCount[playerID] >= scoreToWin)
        {
            GameManager.instance.EndGame();
        }
    }

    public PlayerController GetHighestScorer()
    {
        int currentHighest = 0;
        int playerIndex = 0;

        for (int x = 0; x < elimCount.Length; x++)
        {
            if (elimCount[x] > currentHighest)
            {
                currentHighest = elimCount[x];
                playerIndex = x;
            }
        }
        return GameManager.instance.players[playerIndex];
    }

}
