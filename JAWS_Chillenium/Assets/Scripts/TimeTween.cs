﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeTween : MonoBehaviour
{

    public static void TimeLerp(float scale, float duration)
    {
        MonoBehaviour coroutineHolder = new GameObject().AddComponent<TimeTween>();
        coroutineHolder.hideFlags = HideFlags.HideInHierarchy;
        coroutineHolder.StartCoroutine(TimeEffect(coroutineHolder, scale, duration));
    }

    private static IEnumerator TimeEffect(MonoBehaviour holder, float scale, float duration)
    {
        float timer = 0;
        Time.timeScale = scale;

        while (timer < duration)
        {
            timer += Time.fixedDeltaTime;
            yield return null;
        }

        Time.timeScale = 1;
        Destroy(holder);
        yield break;
    }
}
