﻿using System.Collections;
using System.Collections.Generic;
using FMODUnity;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    StudioEventEmitter music;

    public static SoundManager instance;

    private void Awake()
    {
        if (instance == null)
        instance = this;
    }

    private void Start()
    {
        music = new StudioEventEmitter();
        ChangeMusic(AudioDirectory.SanaMusic);
    }

    public void ChangeMusic(string SoundPath)
    {
        music.Event = SoundPath;
        music.Play();
    }

    public void PlayOneShot(string SoundPath)
    {
        RuntimeManager.PlayOneShot(SoundPath);
    }

}

public class AudioDirectory
{
    public const string PaperKill = "event:/PaperKill";
    public const string RockKill = "event:/RockKill";
    public const string ScissorKill = "event:/ScissorKill";
    public const string Music = "event:/Music";
    public const string SanaMusic = "event:/MusicSana";
    public const string TumbleWeed = "event:/TumbleWeed";
    public const string HawkScreech = "event:/HawkScreech";
    public const string RockSlide = "event:/RockSlide";
    public const string Scream = "event:/Scream";
}
