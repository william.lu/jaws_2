﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public static SpawnManager instance;


    [SerializeField]
    public List<Transform> spawnPoints;

    [SerializeField]
    GameObject spawnParticle;

    [SerializeField]
    float respawnTime = 2;

    [SerializeField]
    float invinTime = 1;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            return;
        }
        Destroy(this);
    }

    IEnumerator KillThenRespawn(PlayerController player)
    {
        Killed(player);

        yield return new WaitForSeconds(0.3f);

        player.gameObject.SetActive(false);

        yield return new WaitForSeconds(respawnTime);

        player.gameObject.SetActive(true);

        Respawn(player);

        yield return new WaitForSeconds(invinTime);

        Vulnerable(player);

        yield break;
    }

    void Killed(PlayerController player)
    {

        player.enabled = false;
        player.attackComponent.enabled = false;
        player.dead = true;

        player.deathParticle.Play();
    }

    void Respawn(PlayerController player)
    {
        player.enabled = true;
        player.spawnParticle.Play();

        player.spawnShield.SetActive(true);

        List<Transform> playerGameObjs = new List<Transform>();

        for (int x = 0; x < GameManager.instance.players.Length; x++)
        {
            if (GameManager.instance.players[x] != null)
            {
                playerGameObjs.Add(GameManager.instance.players[x].gameObject.transform);
            }
        }
        
        Vector3 averageDistance = Distance.GetAveragePos(playerGameObjs);

        Transform furthestSpawnPoint = Distance.GetFurthest(spawnPoints, averageDistance);


        Instantiate(spawnParticle, furthestSpawnPoint.transform.position, Quaternion.identity);

        player.transform.position = furthestSpawnPoint.position;
    }

    void Vulnerable(PlayerController player)
    {
        player.spawnShield.SetActive(false);
        player.attackComponent.enabled = true;
        player.dead = false;
    }

    public void KillPlayer(PlayerController player)
    {
        StartCoroutine(KillThenRespawn(player));
    }

}
