﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateAttack : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision)
    {

        PlayerController otherPlayer = collision.gameObject.GetComponent<PlayerController>();
        PlayerController self = GetComponent<PlayerController>();

        if (otherPlayer != null)
        {
            if (otherPlayer.dead == false && self.dead == false)
            {
                Debug.Log(self);
                otherPlayer.Attacked(self);
                Debug.Log(self);
            }
        }
    }

}
