﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    TextMeshProUGUI TutorialPrompt;
    TextMeshProUGUI pressAPrompt;
    TextMeshProUGUI startPrompt;
    TextMeshProUGUI winnerPrompt;
    TextMeshProUGUI restartPrompt;
    TextMeshProUGUI skipPrompt;

    InputReceiver inputReceiver;


    Dictionary<GameObject, Vector3> UIScales;
    Dictionary<GameObject, Color> UIColors;

    private bool restartingGame = false;

    public Color red;
    public Color yellow;
    public Color blue;

    bool skip = false;


    private void Awake()
    {
        UIScales = new Dictionary<GameObject, Vector3>();
        UIColors = new Dictionary<GameObject, Color>();
        //ghetto af but make sure "tutorialPrompt" is the first child
        TutorialPrompt = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        pressAPrompt = transform.GetChild(1).GetComponent<TextMeshProUGUI>();
        startPrompt = transform.GetChild(2).GetComponent<TextMeshProUGUI>();
        winnerPrompt = transform.GetChild(3).GetComponent<TextMeshProUGUI>();
        restartPrompt = transform.GetChild(4).GetComponent<TextMeshProUGUI>();
        skipPrompt = transform.GetChild(5).GetComponent<TextMeshProUGUI>();

        inputReceiver = GetComponent<InputReceiver>();
    }

    // Use this for initialization
    void Start()
    {
        skipPrompt.gameObject.SetActive(false);
        StartCoroutine(TutorialSequence());

    }


    // Update is called once per frame
    private void Update()
    {
        //check for win
        //shitty to put this in update but who cares
        if (GameManager.instance.focusOnWinner == true && restartingGame == false)
        {
            restartingGame = true;
            StartCoroutine(RestartingGame());
        }

        //tutorial skipping
        if (skip == false)
        {
            inputReceiver.HoldAToSkip();
        }

    }


    IEnumerator TutorialSequence()
    {
        TutorialPrompt.gameObject.SetActive(false);
        //wait one frame to not conflict with void Start
        yield return null;

        float scaleDownTime = 0.7f;


        //LOADING IN
        ChangeTo("press <color=#5CDB72>A</color> to join!", pressAPrompt, 1);
        Hover(pressAPrompt.transform);
        //to be used to the start button once
        bool ranOnce = false;
        while (inputReceiver.currentPhase == InputReceiver.Phase.waitForAPress)
        {
            inputReceiver.TutorialPromptCheck();

            if (inputReceiver.canPressStart && ranOnce == false)
            {
                ranOnce = true;
                startPrompt.gameObject.SetActive(true);
                ChangeTo("", startPrompt, 1);
                Hover(startPrompt.transform);
            }
            yield return null;
        }
        StopHover(startPrompt.transform);
        scaleDownUI(startPrompt, scaleDownTime);

        StopHover(pressAPrompt.transform);
        scaleDownUI(pressAPrompt, scaleDownTime);
        yield return new WaitForSeconds(scaleDownTime);


        yield return new WaitForSeconds(0.5f);



        TutorialPrompt.gameObject.SetActive(true);
        //TUTORIAL
        ChangeTo("okay, let's learn the controls!", TutorialPrompt, 1);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, scaleDownTime);
        yield return new WaitForSeconds(scaleDownTime);




        inputReceiver.InitializeTutorial();

        //let skipping
        skipPrompt.gameObject.SetActive(true);
        ChangeTo("", skipPrompt, 1);
        //ROCK
        //FF554C
        ChangeTo("press <b><color=#FF554C>B</b></color> to become a rock!", TutorialPrompt, 1);
        Hover(TutorialPrompt.transform);
        //WAIT UNTIL EVERYONE PRESSES B
        while (inputReceiver.currentPhase == InputReceiver.Phase.Rock)
        {
            inputReceiver.TutorialPromptCheck();
            yield return null;
        }
        StopHover(TutorialPrompt.transform);
        scaleDownUI(TutorialPrompt, scaleDownTime);
        yield return new WaitForSeconds(scaleDownTime);



        //PAPER
        //FBFF21
        ChangeTo("press <b><color=#FBFF21>Y</b></color> to become paper!", TutorialPrompt, 1);
        Hover(TutorialPrompt.transform);
        //WAIT UNTIL EVERYONE PRESSES Y
        while (inputReceiver.currentPhase == InputReceiver.Phase.Paper)
        {
            inputReceiver.TutorialPromptCheck();
            yield return null;
        }
        StopHover(TutorialPrompt.transform);
        scaleDownUI(TutorialPrompt, scaleDownTime);
        yield return new WaitForSeconds(scaleDownTime);



        //SCISSORS
        //8B9DFF
        ChangeTo("press <b><color=#8B9DFF>X</b></color> to become scissors!", TutorialPrompt, 1);
        Hover(TutorialPrompt.transform);
        //WAIT UNTIL EVERYONE PRESSES X
        while (inputReceiver.currentPhase == InputReceiver.Phase.Scissors)
        {
            inputReceiver.TutorialPromptCheck();
            yield return null;
        }
        StopHover(TutorialPrompt.transform);
        scaleDownUI(TutorialPrompt, scaleDownTime);
        yield return new WaitForSeconds(scaleDownTime);

        yield return new WaitForSeconds(0.5f);




        StartCoroutine(PracticeSequence());


  



        //CHANGE THIS BACK

    }



    IEnumerator PracticeSequence()
    {
        ChangeTo("okay, let's practice transforming.", TutorialPrompt, 1);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2f);
        StopHover(TutorialPrompt.transform);
        scaleDownUI(TutorialPrompt, 1);
        yield return new WaitForSeconds(1);


        /*
        //FIRST THREE
        ChangeTo("press <color=#FF554C>B</color> for <color=#FF554C>ROCK</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(5f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.3f);
        yield return new WaitForSeconds(0.3f);

        ChangeTo("press <color=#FBFF21>Y</color> for <color=#FBFF21>PAPER</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(4f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.3f);
        yield return new WaitForSeconds(0.3f);

        ChangeTo("press <color=#8B9DFF>X</color> for <color=#8B9DFF>SCISSORS</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(4f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.3f);
        yield return new WaitForSeconds(0.3f);
        */

        //Level 2

        //<color=#FBFF21>Y</color> for 
        ChangeTo("<color=#FBFF21>PAPER</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2.5f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.2f);
        yield return new WaitForSeconds(0.2f);
        //<color=#8B9DFF>X</color> for 
        ChangeTo("<color=#8B9DFF>SCISSORS</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2.5f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.2f);
        yield return new WaitForSeconds(0.2f);
        //<color=#FBFF21>Y</color> for 
        ChangeTo("<color=#FBFF21>PAPER</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.2f);
        yield return new WaitForSeconds(0.2f);
        //<color=#FF554C>B</color> for 
        ChangeTo("<color=#FF554C>ROCK</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.1f);
        yield return new WaitForSeconds(0.1f);


        //level 3
        //<color=#FBFF21>Y</color> for 
        ChangeTo("<color=#FBFF21>PAPER</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(1.5f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.1f);
        yield return new WaitForSeconds(0.1f);
        //<color=#8B9DFF>X</color> for 
        ChangeTo("<color=#8B9DFF>SCISSORS</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(1.5f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.1f);
        yield return new WaitForSeconds(0.1f);
        //<color=#FBFF21>Y</color> for 
        ChangeTo("<color=#FBFF21>PAPER</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(1f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.1f);
        yield return new WaitForSeconds(0.1f);
        //<color=#FF554C>B</color> for 
        ChangeTo("<color=#FF554C>ROCK</color>", TutorialPrompt, 0.5f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(1f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 2f);
        yield return new WaitForSeconds(2f);

        
        StartCoroutine(StartGame());
    }

    IEnumerator StartGame()
    {
        ChangeTo("not bad! one more thing though..", TutorialPrompt, 1f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 1f);
        yield return new WaitForSeconds(1f);

        FadeIn("you must...", TutorialPrompt, 1f);
        Hover(TutorialPrompt.transform);
        yield return new WaitForSeconds(2f);

        //disable skip
        skip = true;
        fadeOutUI(skipPrompt, 1f);

        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 1f);
        //lower platforms
        GameManager.instance.RetractSpawnPlatforms();
        yield return new WaitForSeconds(1f);

        ChangeTo("<color=#FF2D3D>FIGHT to the DEATH</color>", TutorialPrompt, 0.5f);
        Shake(TutorialPrompt.transform);
        yield return new WaitForSeconds(4f);
        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 2f);
        yield return new WaitForSeconds(1f);

        GameManager.instance.StartMatch();
    }



    IEnumerator RestartingGame()
    {
        //count down
        float timer = 5;
        winnerPrompt.gameObject.SetActive(true);
        restartPrompt.gameObject.SetActive(true);
        ChangeTo("WINNER!", winnerPrompt, 0.5f);
        ChangeTo("", restartPrompt, 0.5f);
        Hover(winnerPrompt.transform);
        Hover(restartPrompt.transform);
        while (timer >= 0)
        {
            timer -= Time.deltaTime;
            restartPrompt.text = "restarting in... " + Mathf.RoundToInt(timer);
            yield return null;
        }
        StopHover(winnerPrompt.transform);
        StopHover(restartPrompt.transform);
        fadeOutUI(winnerPrompt, 0.5f);
        fadeOutUI(restartPrompt, 0.5f);
        restartPrompt.gameObject.SetActive(false);

        restartingGame = false;


        GameManager.instance.GameSetup(true);
        GameManager.instance.StartMatch();

        yield return new WaitForSeconds(0.5f);
        ChangeTo("again!", winnerPrompt, 0.5f);
        Hover(winnerPrompt.transform);
        yield return new WaitForSeconds(2);
        StopHover(winnerPrompt.transform);
        fadeOutUI(winnerPrompt, 0.5f);
        yield return new WaitForSeconds(0.5f);
        winnerPrompt.gameObject.SetActive(false);

    }

    public IEnumerator skipTutorial()
    {
        skip = true;
        StopAllCoroutines();

        StopHover(TutorialPrompt.transform);
        fadeOutUI(TutorialPrompt, 0.5f);
        yield return new WaitForSeconds(1f);
        fadeOutUI(skipPrompt, 0.2f);
        yield return new WaitForSeconds(0.3f);

        ChangeTo("skipped!", skipPrompt, 0.5f);
        if (skipPrompt.GetComponentInChildren<Image>()!=null)
        skipPrompt.GetComponentInChildren<Image>().gameObject.SetActive(false);
        Hover(skipPrompt.transform);

        ChangeTo("GET READY TO FIGHT!", TutorialPrompt, 0.5f);
        GameManager.instance.RetractSpawnPlatforms();


        yield return new WaitForSeconds(2);
        GameManager.instance.StartMatch();
        scaleDownUI(TutorialPrompt, 0.5f);
        yield return new WaitForSeconds(2f);
        StopHover(skipPrompt.transform);
        fadeOutUI(skipPrompt, 0.5f);

    }

    void ChangeTo(string targetText, TextMeshProUGUI text, float time)
    {
        if (targetText != "")
        {
            //only change if text is different than before
            text.text = targetText;
        }
        //reset text colour
        if (UIColors.ContainsKey(text.gameObject))
        {
            text.color = UIColors[text.gameObject];
        }
        //rotate slightly
        text.transform.DOPunchRotation(new Vector3(0, 0, Mathf.Abs(Random.Range(-3f, 3f))), time, 1).SetEase(Ease.OutBack);
        //scale up
        if (!UIScales.ContainsKey(text.gameObject))
        {
            UIScales.Add(text.gameObject, text.transform.localScale);
        }
        text.transform.localScale = Vector3.zero;
        text.transform.DOScale(UIScales[text.gameObject], time).SetEase(Ease.OutBack);
    }

    void FadeIn(string targetText, TextMeshProUGUI text, float time)
    {
        if (targetText != "")
        {
            //only change if text is different than before
            text.text = targetText;
        }

        if (!UIColors.ContainsKey(text.gameObject))
        {
            UIColors[text.gameObject] = text.color;
        }

        text.color = new Color(text.color.r, text.color.g, text.color.b, 0);

        text.DOFade(1, time);
    }

    void Hover(Transform text)
    {
        if (UIScales.ContainsKey(text.gameObject))
            text.DOScale(UIScales[text.gameObject] * 1.04f, 1.5f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo).SetDelay(1);
    }

    void Shake(Transform text)
    {
        text.DOPunchRotation(new Vector3(0, 0, Mathf.Abs(Random.Range(-5f, 5f))), 5, 10).SetEase(Ease.InOutSine);

        if (!UIScales.ContainsKey(text.gameObject))
        {
            UIScales.Add(text.gameObject, text.transform.localScale);
        }

        //scale faster if you wanna be less funny
        text.DOScale(UIScales[text.gameObject] * 1.25f, 5).SetEase(Ease.OutBack);
    }

    void StopHover(Transform text)
    {
        text.DOKill();
    }

    void scaleDownUI(TextMeshProUGUI text, float time)
    {

        text.transform.DOScale(Vector3.zero, time).SetEase(Ease.InCubic);

        if (!UIColors.ContainsKey(text.gameObject))
        {
            UIColors[text.gameObject] = text.color;
        }

        text.DOFade(0, time);
    }

    void fadeOutUI(TextMeshProUGUI text, float time)
    {
        if (!UIColors.ContainsKey(text.gameObject))
        {
            UIColors[text.gameObject] = text.color;
        }

        text.DOFade(0, time);
    }
}
