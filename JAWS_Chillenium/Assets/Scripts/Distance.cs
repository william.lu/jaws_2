﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Distance : MonoBehaviour
{
    public static GameObject GetClosest(List<GameObject> objs, GameObject target)
    {
        float distance = Mathf.Infinity;
        GameObject closestObj = null;

        for (int x = 0; x < objs.Count; x++)
        {
            if (Vector3.Distance(objs[x].transform.position, target.transform.position) < distance)
            {
                distance = Vector3.Distance(objs[x].transform.position, target.transform.position);
                closestObj = objs[x];
            }
        }
        return closestObj;
    }

    public static Transform GetFurthest(List<Transform> objs, Vector3 target)
    {
        float distance = 0;
        Transform furthestObj = null;

        for (int x = 0; x < objs.Count; x++)
        {
            if (Vector3.Distance(objs[x].position, target) > distance)
            {
                distance = Vector3.Distance(objs[x].position, target);
                furthestObj = objs[x];
            }
        }
        return furthestObj;
    }

    public static Vector3 GetAveragePos(List<Transform> objs)
    {
        Vector3 averagePos = Vector3.zero;
        for (int x = 0; x < objs.Count; x++)
        {
            averagePos += objs[x].position;
            averagePos /= objs.Count;
        }
        return averagePos;
    }
}
